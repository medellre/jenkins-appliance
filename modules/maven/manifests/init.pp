#-----------------------------------------------------------------------------
#   Copyright � 2013 Rene Medellin
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#-----------------------------------------------------------------------------

class maven {

  file { '/opt/downloads/apache-maven':
        ensure => 'directory',
        mode => '0777',
  }

  downloads::wget { 'apache-maven-3.0.5-bin.tar.gz':
        url => 'http://www.apache.org/dist/maven/maven-3/3.0.5/binaries/apache-maven-3.0.5-bin.tar.gz',
        target_dir => '/opt/downloads/apache-maven',
        creates => '/opt/downloads/apache-maven/apache-maven-3.0.5-bin.tar.gz',
  }

  downloads::untargz { '/opt/downloads/apache-maven/apache-maven-3.0.5-bin.tar.gz':
        target_dir => '/opt',
        creates => '/opt/apache-maven-3.0.5',
        subscribe => Downloads::Wget['apache-maven-3.0.5-bin.tar.gz'],
  }
  
  file { '/opt/apache-maven-3.0.5':
        ensure => directory,
        require => Downloads::Untargz['/opt/downloads/apache-maven/apache-maven-3.0.5-bin.tar.gz'],
  }
}
