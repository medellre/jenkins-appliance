#-----------------------------------------------------------------------------
#   Copyright � 2013 Rene Medellin
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#-----------------------------------------------------------------------------

class jenkinsplugins {
  $jenkins_install_root = $operatingsystem ? {
    CentOS  => '/var/lib/jenkins/plugins',
    default => '/opt/jenkins-unknown-rootdir/plugins', 
  }

  file { $jenkins_install_root:
    ensure => directory,
    mode   => 0644,
    owner  => 'jenkins',
    require => Package["${jenkins::package}"],
  }

  $recommended_plugins = [
    "active-directory.jpi",
    "analysis-collector.jpi",
    "analysis-core.jpi",
    "ant.jpi",
    "artifactory.jpi",
    "audit-trail.jpi",
    "build-pipeline-plugin.jpi",
    "checkstyle.jpi",
    "claim.jpi",
    "cobertura.jpi",
    "copyartifact.jpi",
    "credentials.jpi",
    "custom-job-icon.jpi",
    "cvs.jpi",
    "dashboard-view.jpi",
    "disk-usage.jpi",
    "email-ext.jpi",
    "emma.jpi",
    "envinject.jpi",
    "extended-choice-parameter.jpi",
    "extended-read-permission.jpi",
    "extensible-choice-parameter.jpi",
    "external-monitor-job.jpi",
    "extra-columns.jpi",
    "filesystem_scm.jpi",
    "findbugs.jpi",
    "git-client.jpi",
    "git.jpi",
    "github-api.jpi",
    "github.jpi",
    "greenballs.jpi",
    "ivy.jpi",
    "javadoc.jpi",
    "jenkins-jira-issue-updater.jpi",
    "jira.jpi",
    "join.jpi",
    "ldap.jpi",
    "locks-and-latches.jpi",
    "m2release.jpi",
    "mailer.jpi",
    "mask-passwords.jpi",
    "maven-plugin.jpi",
    "pam-auth.jpi",
    "parameterized-trigger.jpi",
    "performance.jpi",
    "pmd.jpi",
    "powershell.jpi",
    "progress-bar-column-plugin.jpi",
    "publish-over-ssh.jpi",
    "role-strategy.jpi",
    "saferestart.jpi",
    "slave-status.jpi",
    "sonar.jpi",
    "ssh-credentials.jpi",
    "ssh-slaves.jpi",
    "subversion.jpi",
    "token-macro.jpi",
    "translation.jpi",
    "ws-cleanup.jpi",
  ]

  define installplugins() {
    file { "${jenkinsplugins::jenkins_install_root}/${name}":
      ensure => present,
      mode   => 0644,
      owner  => jenkins,
      source => "puppet:///modules/jenkinsplugins/${name}",
      require => [
        File[$jenkinsplugins::jenkins_install_root],
        Package['jenkins'],
      ],
      notify => Service[$jenkins::package],
    }
  }

  installplugins { $recommended_plugins: }
}
