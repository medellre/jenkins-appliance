#-----------------------------------------------------------------------------
#   Copyright � 2013 Rene Medellin
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#-----------------------------------------------------------------------------

class jenkins {
  # This is the main module for Jenkins
  require motd
  require iptables
  require downloads
  require unzip
  require java
  require subversion
  require git
  require mercurial
  require ant
  require maven

  $package = $::operatingsystem ? {
    CentOS  => 'jenkins',
    default => 'jenkins',
  }

  yumrepo { $package:
    baseurl  => "http://pkg.jenkins-ci.org/redhat",
    descr    => "jenkins-ci.org",
    enabled  => 1,
    gpgcheck => 1,
    gpgkey   => "http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key",
  }

  package { $package:
    ensure => latest,
    require => Yumrepo[$package],
  }

  service { $package:
  	ensure => running,
  	enable => true,
  	require => Package[$package],
  }

}
