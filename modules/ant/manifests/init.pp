#-----------------------------------------------------------------------------
#   Copyright � 2013 Rene Medellin
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#-----------------------------------------------------------------------------

class ant {

  file { '/opt/downloads/apache-ant':
        ensure => 'directory',
        mode => '0777',
  }

  downloads::wget { 'apache-ant-1.9.1-bin.tar.gz':
        url => 'http://www.apache.org/dist/ant/binaries/apache-ant-1.9.1-bin.tar.gz',
        target_dir => '/opt/downloads/apache-ant',
        creates => '/opt/downloads/apache-ant/apache-ant-1.9.1-bin.tar.gz',
  }

  downloads::untargz { '/opt/downloads/apache-ant/apache-ant-1.9.1-bin.tar.gz':
        target_dir => '/opt',
        creates => '/opt/apache-ant-1.9.1',
        subscribe => Downloads::Wget['apache-ant-1.9.1-bin.tar.gz'],
  }
  
  file { '/opt/apache-ant-1.9.1':
        ensure => directory,
        require => Downloads::Untargz['/opt/downloads/apache-ant/apache-ant-1.9.1-bin.tar.gz'],
  }
}
